<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main role="main">
			<section class="beforeAfter" id="morePadding">

				<div class="baWrapper">
					<div class="header">
						<h2>Portfolio</h2>
						<p><?php single_cat_title(); ?></p>
					</div>
				</div>

			    <!-- Swiper -->
			    <div class="transformation">
				    <div class="swiper-container">
				        <div class="swiper-wrapper">

        	            <?php
                                $args = array(
                          'post_type' => 'portfolio_gallery',
                          'posts_per_page' => '99'
                          );
                                $products = new WP_Query( $args );
                                      if( $products->have_posts() ) {
                                while( $products->have_posts() ) {
                                $products->the_post();
                          ?>
				            <div class="swiper-slide" style="background-image:url(<?php the_field('image'); ?>)">
				            	<!-- <img src="<//?php the_field('image'); ?>"> -->
				            	<div class="transformCaption">
					            	<h6><?php the_field('image_title'); ?></h6>
					            	<p><?php the_field('image_caption'); ?></p>
				            	</div>
				            </div>

                         <?php
                                }
                                      }
                                else {
                                echo 'No Images Found';
                                }
                          ?>

				        </div>
				        <!-- Add Pagination -->
				        <div class="swiper-pagination"></div>
				        <!-- Add Arrows -->
				        <div class="swiper-button-next"></div>
				        <div class="swiper-button-prev"></div>
				    </div>
			    </div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    slidesPerView: 2,
    paginationClickable: true,
    height:700,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 5,
    breakpoints: {
	    870: {
	        slidesPerView: 1
	    }
	}
});
</script>
<?php
get_footer();
