<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main role="main">
			<section class="registry">
				<div class="registryWrapper">
					<div class="header">
						<h2>Registry</h2>
						<p>Contact Michele or Register for a makeup <br>or hairstyle consultation before your wedding day.</p>
					</div>
				</div>

				<div class="aboutUsContent">
					<div class="aboutUsContentWrapper">
						<div class="aboutUsSidebar">
							<div class="facebookBlock">
								<h4><span class="pinkFont">Like us on<br> Facebook</span></h4>
								<p>For daily updates, stories and beauty tips like you've never heard before.</p>
								<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FBridal-By-Michele-116151728455206%2F&width=88&layout=button_count&action=like&size=large&show_faces=true&share=false&height=21&appId=1789372451336750" width="88" height="30" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							</div>

							<div class="sidebarBlock">
								<div class="blockContent">
									<p>Bridal by Michele provides full hair and makeup service for Weddings, Fashion, Quinceañera Parties, Bar/Bat Mitzvahs, Sweet Sixteen or any other event you would want a professional, personalized makeup service.</p>
								</div>
								<div class="blockContent">
									<h6>Where</h6>
									<p>New Jersey, New York, Connecticut & Tri-State Area</p>
								</div>	
								<div class="blockContent">
									<h6>Hours</h6>
									<p>10:00a-7:00p, Mon-Tue <br>2:00p-6:00p, Sat <br>10:00a-7:00p, Sun</p>
								</div>	
								<div class="callToAction">
									<a href="" class="bookBtn">
										<button>Book An Appointment</button>
									</a>
									<a href="" class="contactBtn">
										<button>Call</button>
									</a>			
									<a href="" class="contactBtn" id="emailBtn">
										<button>Email</button>
									</a>
								</div>													
							</div>
						</div>
						<div class="registryMain">
							<div class="registryForm">
								<div class="contactContainer">
									<?php echo do_shortcode('[contact-form-7 id="35" title="Registry"]'); ?>
								</div>
							</div>
						</div>

					</div>
				</div>

			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    slidesPerView: 2,
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 5,
    breakpoints: {
	    870: {
	        slidesPerView: 1
	    }
	}
});
</script>
<?php
get_footer();
