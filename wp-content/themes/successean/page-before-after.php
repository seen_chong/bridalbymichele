<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main role="main">
			<section class="beforeAfter">

				<div class="baWrapper">
					<div class="header">
						<h2>Before & After</h2>
						<p>Look through incredible transformations,<br>from beauty to unique & natural beauty. </p>
					</div>
				</div>

			    <!-- Swiper -->
			    <div class="transformation">
				    <div class="swiper-container">
				        <div class="swiper-wrapper">

				          <?php
                                $args = array(
                          'post_type' => 'before_and_after',
                          'posts_per_page' => '99'
                          );
                                $products = new WP_Query( $args );
                                      if( $products->have_posts() ) {
                                while( $products->have_posts() ) {
                                $products->the_post();
                          ?>
				            <div class="swiper-slide">
				            	<img src="<?php the_field('image'); ?>">
				            	<div class="transformCaption">
					            	<h6><?php the_field('image_title'); ?></h6>
					            	<p><?php the_field('image_caption'); ?></p>
				            	</div>
				            </div>

                         <?php
                                }
                                      }
                                else {
                                echo 'No Images Found';
                                }
                          ?>
				        </div>
				        <!-- Add Pagination -->
				        <div class="swiper-pagination"></div>
				        <!-- Add Arrows -->
				        <div class="swiper-button-next"></div>
				        <div class="swiper-button-prev"></div>
				    </div>
			    </div>
			</section>

			<section class="booking">
				<div class="bookingHeader">
					<h4>Reserve your appointment now <br>before someone else books your time</h4>
					<a href="/registry" id="appointment"><button>Make an Appointment</button></a>
					<a href="/services" id="learnMore"><button>Learn More</button></a>
				</div>
			</section>

			<section class="contact">
				<div class="contactContainer">
					<?php echo do_shortcode('[contact-form-7 id="27" title="Contact Us"]'); ?>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    slidesPerView: 1,
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 5,
    breakpoints: {
	    870: {
	        slidesPerView: 1
	    }
	}
});
</script>
<?php
get_footer();
