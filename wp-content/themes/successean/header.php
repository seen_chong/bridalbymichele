<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package successean
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/libs/swiper/swiper.css">
<link href="//cdn.rawgit.com/noelboss/featherlight/1.6.1/release/featherlight.min.css" type="text/css" rel="stylesheet" />

<!-- Jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


<script src="//cdn.rawgit.com/noelboss/featherlight/1.6.1/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/libs/swiper/swiper.min.js"></script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'successean' ); ?></a>

	<header id="masthead" class="site-header" role="banner">


			<div class="headerContainer">

				<!-- Site Branding -->
				<img id="flowerLeft" src="<?php echo get_template_directory_uri(); ?>/assets/img/flowers-left.png">
				<img id="flowerRight" src="<?php echo get_template_directory_uri(); ?>/assets/img/flowers-right.png">


				<div class="headerContent">				
					<div class="headerLeft">
						<h1 id="siteLogo">
							<a href="/">
								<span class="pinkFont">Bridal</span> by <span class="italics">Michele</span>
							</a>
						</h1>
						<div>
							<p>Hair</p>
							<p>Makeup</p>
							<p>Airbrush</p>
						</div>
					</div>
					<div class="headerRight">
						<div class="callToAction">

							<p class="desktopView"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-phone.png">NJ <a href="tel:908-309-5523"><span class="pinkFont">908.309.5523</span></a> + NYC <a href="tel:917-757-6768">917.757.6768</a></p>
							<p class="mobileView">NJ <a href="tel:908-309-5523"><span class="pinkFont">908.309.5523</span></a> <br> NYC <a href="tel:917-757-6768">917.757.6768</a></p>
							<div class="callToActionButtons">
								<a href="/registry" class="contactBtn">
									<button>Registry</button>
								</a>			
								<a href="mailto:michele@bridalbymichele.com?Subject=Hello" target="_top" class="contactBtn" id="emailBtn">
									<button>Email</button>
								</a>
							</div>
						</div>	
				</div>
					
				</div>
				
			</div>






		<nav class="siteNav" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'successean' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
