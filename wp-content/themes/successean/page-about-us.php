<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main role="main">
			<section class="about">
				<div class="header">
					<h2>About Us</h2>
				</div>

				<div class="aboutNews">
					<div class="aboutNewsWrapper">
						<div class="aboutNewsLeft">
							<ul>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-makeup.png">Makeup
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-cut.png">Cut
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-color.png">Color</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-blowout.png">Blowout
								</li>
							</ul>
							<a href="/services">
								<button>See Full Services</button>
							</a>
						</div>

						<div class="aboutNewsRight">
							<h4>Join our mailing list today to save 10% off your appointment!</h4>
							<div class="aboutNewsForm">
								<?php echo do_shortcode('[contact-form-7 id="31" title="About Us Newsletter"]'); ?>
							</div>
						</div>
					</div>
				</div>

				<div class="aboutUsContent">
					<div class="aboutUsContentWrapper">
						<div class="aboutUsSidebar">
							<div class="facebookBlock">
								<h4><span class="pinkFont">Like us on<br> Facebook</span></h4>
								<p>For daily updates, stories and beauty tips like you've never heard before.</p>
								<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FBridal-By-Michele-116151728455206%2F&width=88&layout=button_count&action=like&size=large&show_faces=true&share=false&height=21&appId=1789372451336750" width="88" height="30" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							</div>

							<div class="sidebarBlock">
								<div class="blockContent">
									<p>Bridal by Michele provides full hair and makeup service for Weddings, Fashion, Quinceañera Parties, Bar/Bat Mitzvahs, Sweet Sixteen or any other event you would want a professional, personalized makeup service.</p>
								</div>
								<div class="blockContent">
									<h6>Where</h6>
									<p>New Jersey, New York, Connecticut & Tri-State Area</p>
								</div>	
								<div class="blockContent">
									<h6>Hours</h6>
									<p>10:00a-7:00p, Mon-Tue <br>2:00p-6:00p, Sat <br>10:00a-7:00p, Sun</p>
								</div>	
								<div class="callToAction">
									<a href="/registry" class="bookBtn">
										<button>Book An Appointment</button>
									</a>
									<a href="tel:908-309-5523" class="contactBtn">
										<button>Call</button>
									</a>			
									<a href="mailto:michele@bridalbymichele.com?Subject=Hello" target="_top" class="contactBtn" id="emailBtn">
										<button>Email</button>
									</a>
								</div>													
							</div>
						</div>
						<div class="aboutUsMain">
							<h4><?php the_field('top_header'); ?></h4>
							<img src="<?php the_field('michele_portrait'); ?>">
						</div>

						<div class="aboutUsMainContent">
							<h4><?php the_field('middle_header'); ?></h4>
							<p><?php the_field('middle_text'); ?></p>			
						</div>
						<div class="aboutUsMainContent" id="aboutUsMainContentImg">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/slide-ex.png">
						</div>
						<div class="aboutUsMainContent">
							<h4><?php the_field('bottom_header'); ?></h4>
							<p><?php the_field('bottom_text'); ?></p>	
						</div>
					</div>
				</div>

			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    slidesPerView: 2,
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 5,
    breakpoints: {
	    870: {
	        slidesPerView: 1
	    }
	}
});
</script>
<?php
get_footer();
