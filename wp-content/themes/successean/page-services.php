<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package successean
 */

get_header(); ?>

	<div id="primary" class="content-area">
			<section class="services">
				<div class="servicesMenu">
					<ul>
						<li>
							<a href="#consultation">Consultation</a>
						</li>
						<li>
							<a href="#makeup">Makeup</a>
						</li>
						<li>
							<a href="#hair">Hair</a>
						</li>
						<li>
							<a href="#eyes">Eyes</a>
						</li>
						<li>
							<a href="#convenience">Convenience</a>
						</li>
					</ul>
				</div>

				<div class="servicesWrapper">

					<div class="servicesBlock" id="consultation">
						<div class="servicesText">
							<h3>Consultation</h3>
							<h5><?php the_field('consultation_intro'); ?></h5>
							<p><?php the_field('consultation_summary'); ?></p>
						</div>
						<div class="servicesImage" style="background-image:url(<?php the_field('consultation_image'); ?>)">

						</div>

					</div>

					<div class="servicesBlock" id="makeup">
						<div class="servicesImage" style="background-image:url(<?php the_field('makeup_image'); ?>)">

						</div>
						<div class="servicesText">
							<h3>Makeup</h3>
							<h5><?php the_field('makeup_intro'); ?></h5>
							<p><?php the_field('makeup_summary'); ?></p>
						</div>
					</div>
				</div>
					<div class="servicesBreak">

					<img id="flowerLeft" src="<?php echo get_template_directory_uri(); ?>/assets/img/flowers-left.png">
					<img id="flowerRight" src="<?php echo get_template_directory_uri(); ?>/assets/img/flowers-right.png">
					<div class="servicesBreakWrapper">
						<div class="sidebarBlock">
							<div class="blockContent">
								<h6>Where</h6>
								<p>New Jersey, New York, Connecticut & Tri-State Area</p>
							</div>	
							<div class="blockContent">
								<h6>Hours</h6>
								<p>10:00a-7:00p, Mon-Tue <br>2:00p-6:00p, Sat <br>10:00a-7:00p, Sun</p>
							</div>	
							<div class="callToAction">
								<a href="/registry" class="bookBtn">
									<button>Book An Appointment</button>
								</a>
								<a href="tel:908-309-5523" class="contactBtn">
									<button>Call</button>
								</a>			
								<a href="mailto:michele@bridalbymichele.com?Subject=Hello" target="_top" class="contactBtn" id="emailBtn">
									<button>Email</button>
								</a>
							</div>				
						</div>
						<div class="facebookBlock">
								<h4><span class="pinkFont">Like us on<br> Facebook</span></h4>
								<p>For daily updates, stories and beauty tips like you've never heard before.</p>
								<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FBridal-By-Michele-116151728455206%2F&width=88&layout=button_count&action=like&size=large&show_faces=true&share=false&height=21&appId=1789372451336750" width="88" height="30" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							</div>
						</div>
					</div>

				<div class="servicesWrapper">
					<div class="servicesBlock" id="hair">
						<div class="servicesText">
							<h3>Hair</h3>
							<h5><?php the_field('hair_intro'); ?></h5>
							<p><?php the_field('hair_summary'); ?></p>
						</div>
						<div class="servicesImage" style="background-image:url(<?php the_field('hair_image'); ?>)">

						</div>

					</div>

					<div class="servicesBlock" id="eyes">
						<div class="servicesImage" style="background-image:url(<?php the_field('eyes_image'); ?>)">

						</div>
						<div class="servicesText">
							<h3>Eyes</h3>
							<h5><?php the_field('eyes_intro'); ?></h5>
							<p><?php the_field('eyes_summary'); ?></p>
						</div>
					</div>
					<div class="servicesBlock" id="convenience">
						<div class="servicesText">
							<h3>Convenience</h3>
							<h5><?php the_field('convenience_intro'); ?></h5>
							<p><?php the_field('convenience_summary'); ?></p>
						</div>
						<div class="servicesImage" style="background-image:url(<?php the_field('convenience_image'); ?>)">

						</div>

					</div>				
				</div>
			</section>

	</div><!-- #primary -->

<?php
get_footer();
?>
<script type="text/javascript">
$('a[href^="#"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }
});
</script>
