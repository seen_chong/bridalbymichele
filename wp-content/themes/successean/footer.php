<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package successean
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footerContainer">
			<div class="footerText">
				<p>NJ <a href="tel:908-309-5523">908.309.5523</a> + NYC <a href="tel:917-757-6768">917.757.6768</a></p>
<!-- 				<p>&copy;2010-2016 All Rights Reserved</p> -->
				<p>Couture Events + 708 Rt 35 Neptune, NJ 07753</p>
			</div>
			<div class="footerAwards">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/award-badges.png">
			</div>
			<div class="footerSocial">
				<ul>
					<li><a href="https://www.facebook.com/Bridal-By-Michele-116151728455206/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-fb.png"></a></li>
<!-- 					<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social-icon-snap.png"></a></li> -->
					<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social-icon-twitter.png"></a></li>
					<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social-icon-pin.png"></a></li>
					<li><a href="https://www.instagram.com/bridalbymichele/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social-icon-ig.png"></a></li>
				</ul>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
