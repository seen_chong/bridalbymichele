<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package successean
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main role="main">
			<section class="mainBanner" style="background-image:url(<?php the_field('main_banner'); ?>)">
				<div class="mainBannerContent">
					<div class="centerBlock">
						<h1><span class="whiteFont">You De</span>serve</h1> 
						<h2><span class="whiteFont">Unve</span>iled</h2>
						<h3>Beauty</h3> 
						<p><span class="whiteFont">" You should look li</span>ke yourself, <br><span class="whiteFont">only more gorgeou</span>s, <br><span class="whiteFont">on your wedding d</span>ay. "</p>
						<a href="/registry"><button>Make An Appointment</button></a>
					</div>
				</div>

				<div class="mainBannerLeft"></div>

			</section>

			<section class="experiences">

				<div class="experienceWrapper">
					<div class="header">
						<h2>Experiences</h2>
						<p>Read the stories of our brides</p>
					</div>

					<div class="experienceBlock">
						<div class="experienceImage" style="background-image:url(<?php the_field('1st_experience_image'); ?>)">

						</div>
						<div class="experienceText">
							<h6>Hair Eye &#9679; Lashes</h6>
							<h3><?php the_field('1st_experience_client_name'); ?></h3>
							<p><?php the_field('1st_experience_story'); ?></p>
							<a href="/testimonials/"><button>Learn More</button></a>
						</div>
					</div>

					<div class="experienceBlock">
						<div class="experienceText">
							<h6>Makeup Eye &#9679; Lashes</h6>
							<h3><?php the_field('2nd_experience_client_name'); ?></h3>
							<p><?php the_field('2nd_experience_story'); ?></p>
							<a href="/testimonials/"><button>Learn More</button></a>
						</div>
						<div class="experienceImage" style="background-image:url(<?php the_field('2nd_experience_image'); ?>)">

						</div>

					</div>
				</div>
			</section>

			<section class="story">
				<div class="storyContainer" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/pink-stain.png')">
					<div class="storyWrapper">
						<div class="header">
							<p>Est. 1984</p>
							<h2>Our Story</h2>
						</div>
						<div class="storyContent">
							<p><?php the_field('our_story'); ?></p>
						</div>	
					</div>

				</div>
			</section>

			<section class="contact">
				<div class="contactContainer" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/contact-frame.png')">
					<?php echo do_shortcode('[contact-form-7 id="27" title="Contact Us"]'); ?>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
