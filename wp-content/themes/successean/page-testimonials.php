<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main role="main">
			<section class="testimonials">
				<div class="header" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/bg-flower.png')">
					<h2>Testimonials</h2>
				</div>

				<div class="testimonialsBanner" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/bg-test.png')">
					<h6>Overall Rating</h6>
					<div class="starRating">
						<h6>4.7 </h6><img src="<?php echo get_template_directory_uri(); ?>/assets/img/4-stars.png">
					</div>
					<p><span class="pinkFont">4.7 average rating from the</span> <a href="https://www.theknot.com/marketplace/bridal-by-michele-hair-and-airmakeup-west-end-nj-614583" target="blank">The Knot</a>, <a href="https://www.weddingwire.com/biz/bridal-by-michele-long-branch/11e0eb4f4a705ed7.html" target="blank">Wedding Wire</a> <span class="pinkFont">and</span> <a href="">Yelp.com</a></p>
				</div>

			</section>

			<section class="reviews">
				<div class="testimonialGrid">
					<div class="testimonialGridWrapper">
						<div class="testimonialHeader">
							<h6>Showing Great Reviews Here</h6>
						</div>
						<ul>
						<?php
                                $args = array(
                          'post_type' => 'testimonial',
                          'posts_per_page' => 99
                          );
                                $products = new WP_Query( $args );
                                      if( $products->have_posts() ) {
                                while( $products->have_posts() ) {
                                $products->the_post();
                          ?>
                          	<a href="#" data-featherlight="#mylightbox<?php the_field('id'); ?>">
								<li class="popup-gallery">
									<h3><?php the_field('client_name'); ?></h3>
									<h4><?php the_field('occasion'); ?></h4>
									<p><?php the_field('short_testimonial'); ?></p>
								</li>
							</a>
			<div class="mylightbox" id="mylightbox<?php the_field('id'); ?>">
				<div class="testimonialHeader">
					<h3><?php the_field('client_name'); ?></h3>
					<h4><?php the_field('occasion'); ?></h4>
				</div>
				
				<p><?php the_field('short_testimonial'); ?></p>
			</div>
						<?php
                                }
                                      }
                                else {
                                echo 'No Testimonials Found';
                                }
                          ?>					
						</ul>
						<div class="testimonialFooter">
							<h6>For more great reviews, please visit the</h6>
							<p><a href="https://www.theknot.com/marketplace/bridal-by-michele-hair-and-airmakeup-west-end-nj-614583" target="blank">The Knot</a>, <a href="https://www.weddingwire.com/biz/bridal-by-michele-long-branch/11e0eb4f4a705ed7.html" target="blank">Wedding Wire</a> and <a href="">Yelp.com</a></p>
						</div>
					</div>
				</div>
			</section>

			<section class="newsletter">
				<div class="newsletterFloat">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/floatFlower.png">
				</div>
				<div class="newsletterWrapper">
					<div class="callUsContainer">
						<h2>Call Us</h2>
						<p id="numbers">NJ <a href="tel:908-309-5523">908.309.5523</a> - NYC <a href="tel:917-757-6768">917.757.6768</a></p>
						<p></p>
					</div>
					<div class="newsletterContainer">
						<?php echo do_shortcode('[contact-form-7 id="29" title="Newsletter"]'); ?>
					</div>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    slidesPerView: 2,
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 5,
    breakpoints: {
	    870: {
	        slidesPerView: 1
	    }
	}
});
</script>
<?php
get_footer(); ?>


