<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bridalbymichele_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'u_oE%Gzs6`N GKdqS+sXh?1AXOHZwQj03373~3r][AbdP}`*U?[7k6#H8qf*uG.z');
define('SECURE_AUTH_KEY',  '* ^xu!@f4oYa1Y7jfUxah1x>}Fb)(wl<@iK7^{(z0;+%keM4+-l..YBw<`jmM88W');
define('LOGGED_IN_KEY',    'ir=A+%s|~N;=zdPui5$?p4X,~k3jLP]&gt%$p mYpXC8bO[52Ie1eRNf#r7gs$Ae');
define('NONCE_KEY',        'fk2&Z%4AIPQ)$$D3[$RalT2_NNl+>3UklJPB#KFX>WB(iD9rCkO@JdbHcyGEGRB,');
define('AUTH_SALT',        ')[0Ft~S=VxO=QnHK?Z-l<g~(m^75<.~I_y*Y98l;8eq}jm=3a1-1PY_l)xOCG[I%');
define('SECURE_AUTH_SALT', ' l;|m*6LO|EmjNHK)TBYu`EWj71?+kDBfU<TG%ddQIEF1Ng OPg#Q=nqlu<0.WL<');
define('LOGGED_IN_SALT',   '%|p&e%*qLshL5;cGo1G=_y]/aPg!F7n?:>!/((kU4WfYs$LYv-t=P/3sfgqR/svB');
define('NONCE_SALT',       'I5{rb2c_KsXj|;+Pd188AP{Mj1 B3#xqEI#+5}b#~1:=wB*]qM |-NxrJ;x@oHwQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
